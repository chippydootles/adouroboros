# Adouroboros

This project is an attempt to creatively use a 2d touch surface to interact with adressable LEDS. It is heavily inspired by Evil Genious Labs boards, but with an added twist of touch interactivety.
All files in this repository are were made by Martin Yousif.

The project a wip, I ran out of money for testing and my focus needed to be switched up to my anwaar boards, however for the sake of sharing and openeness I'm putting them up in a less than polished form here.

## Boards
* the TOP Board 
	- This board contains the touch matrix and an AVD128DA64 It also works as a diffuser. Work needs to be done to code code the matrix to output information over Serial or I2C. Power, coms, and programming are broken out to the 2x5 header. Initial test suggest the touch matrix does at least work.
* The BOT board.
	- This is the LED containing board, it currently uses apa102 based adressable leds and has large tht pads for power and data/clk. LED control could be integrated but for now I thought it was best to have the led lines broken out to keep it platform agnostic for test and development.